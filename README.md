# localise

[![Build Status](https://git.fmrib.ox.ac.uk/yqzheng1/python-localise/badges/main/pipeline.svg)](https://git.fmrib.ox.ac.uk/yqzheng1/python-localise)
[![Documentation Status](https://img.shields.io/badge/docs-dev-blue.svg)](https://open.win.ox.ac.uk/pages/yqzheng1/python-localise/)
[![Codecov](https://git.fmrib.ox.ac.uk/yqzheng1/python-localise/badges/main/coverage.svg)](https://git.fmrib.ox.ac.uk/yqzheng1/python-localise)

This library implements LOCALISE, a python toolbox developed to address the challenges associated with accurately targeting DBS targets on low-quality clinical-like dataset. This toolbox uses Image Quality Transfer techniques to transfer anatomical information from high-quality data to a wide range of connectivity features in low-quality data. The goal is to augment the inference on DBS targets localisation even with compromised data quality. We also have a Julia implementation [here](https://git.fmrib.ox.ac.uk/yqzheng1/hqaugmentation.jl). For more details, please check our paper [here](https://link.springer.com/chapter/10.1007/978-3-031-43996-4_17).

## License

This project is licensed under the MIT License - see the [LICENSE](./LICENSE) for details.

## Contributing

We greatly appreciate contributions from the community! If you're interested in improving localise, there are many ways you can contribute.

### Reporting Issues

If you're experiencing a problem with localise, please open an issue on the GitHub repository. When submitting an issue, try to include as much detail as you can. This should include the exact steps to reproduce the issue, your operating system, and version of localise.

### Fixing bugs or adding new features

If you're interested in contributing code to fix open issues or adding new features, please follow these steps:

 1. Fork the repository and clone it locally.
 2. Create a branch for your edits.
 3. Add, commit, and push your changes.
 4. Submit a pull request.

When submitting a pull request, please make sure to include a descriptive title and clear description of your changes.

### Documentation

If you spot a problem with the documentation or think it could be clearer, you can make amendments by editing the relevant files and submitting a pull request.

Remember, contributions aren't just about code - any way in which you can help us improve is much appreciated!

Thank you for your interest in contributing to localise!
