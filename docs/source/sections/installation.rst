.. _installation:

Installation
============

To use Localise, first install it using pip:

.. code-block:: console

   (.venv) $ git clone https://git.fmrib.ox.ac.uk/yqzheng1/python-localise.git
   (.venv) $ cd python-localise 
   (.venv) $ pip install .

.. _fsl_installation:

FSL and Freesurfer Installation 
-------------------------------

Before proceeding, make sure you've also installed `FSL <https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/>`_ 
and `Freesurfer <https://surfer.nmr.mgh.harvard.edu/>`_ first, 
and have environment variables ``$FSLDIR`` and ``$FREESURFER_HOME`` set up correctly.