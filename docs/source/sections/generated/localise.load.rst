﻿localise.load
=============

.. automodule:: localise.load

   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
   
      load_data
      load_features
      load_labels
   
   

   
   
   .. rubric:: Classes

   .. autosummary::
   
      CustomDataset
      LargeCustomDataset
      ShuffledDataLoader
   
   

   
   
   



