﻿localise.flatten\_batch
=======================

.. automodule:: localise.flatten_batch

   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
   
      get_adj_sparse
      get_adj_sparse_kdt
   
   

   
   
   .. rubric:: Classes

   .. autosummary::
   
      Adjacency
      FlattenedCRFBatch
      FlattenedCRFBatchTensor
   
   

   
   
   



