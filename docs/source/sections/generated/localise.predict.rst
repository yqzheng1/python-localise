﻿localise.predict
================

.. automodule:: localise.predict

   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
   
      apply_model
      apply_pretrained_model
   
   

   
   
   

   
   
   



