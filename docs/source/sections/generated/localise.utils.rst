﻿localise.utils
==============

.. automodule:: localise.utils

   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
   
      connectivity_driven
      create_masks
      create_tracts
      get_subjects
      predict_mode
      run_command
      save_nifti
      save_nifti_4D
      train_mode
   
   

   
   
   

   
   
   



