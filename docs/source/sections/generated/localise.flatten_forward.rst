﻿localise.flatten\_forward
=========================

.. automodule:: localise.flatten_forward

   
   
   

   
   
   

   
   
   .. rubric:: Classes

   .. autosummary::
   
      Affine
      FlexibleCRF
      FlexibleClassifier
      MLP
      Perceptron
      PerceptronCRF
   
   

   
   
   



