.. _predict_mode:

Tutorials for Localising a Target
=================================

This guide walks you through the process of localising a target using either:

1. Default models provided with this package.
2. Your custom-trained model.

Before proceeding, ensure that the necessary tract-density maps 
related to your selected seed mask have been generated. 
You have the flexibility to carry out the localisation via 
:ref:`Command Line Interface (CLI) <cli-predict>` 
or :ref:`Python Interface <python-predict>`.
Choose the method that fits your needs.

.. toctree::
    :maxdepth: 2
    :caption: Contents:

    tutorials/cli
    tutorials/py
