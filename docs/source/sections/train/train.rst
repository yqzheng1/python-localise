.. _train_mode:

Tutorials for Training a Custom Model
=====================================

This section outlines the steps to train a custom model specific to your data or target. 
Before diving in, ensure you have all the necessary data prepared. 
Refer to :ref:`Prerequisites for training a custom model <prerequisites2>`.

For training, you have two primary methods: 
the :ref:`Command Line Interface (CLI) <cli-train>` or
the :ref:`Python Interface <python-train>`.
Choose the method that best fits your workflow.

.. toctree::
    :maxdepth: 2
    :caption: Contents:

    tutorials/cli
    tutorials/python

