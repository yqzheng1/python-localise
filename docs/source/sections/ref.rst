.. _ref:

References
==========

- Zheng, Ying-Qiu, Harith Akram, Stephen Smith, and Saad Jbabdi. "A Transfer Learning Approach to Localising a Deep Brain Stimulation Target." In International Conference on Medical Image Computing and Computer-Assisted Intervention, pp. 176-185. Cham: Springer Nature Switzerland, 2023.

- Zheng, Shuai, Sadeep Jayasumana, Bernardino Romera-Paredes, Vibhav Vineet, Zhizhong Su, Dalong Du, Chang Huang, and Philip HS Torr. "Conditional random fields as recurrent neural networks." In Proceedings of the IEEE international conference on computer vision, pp. 1529-1537. 2015.